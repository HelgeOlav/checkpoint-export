package checkpoint_export

import "bitbucket.org/HelgeOlav/utils/version"

func init() {
	version.VERSION = "0.0.1"
	version.NAME = "checkpoint_export"
}
