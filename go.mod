module bitbucket.org/HelgeOlav/checkpoint-export

go 1.17

require (
	bitbucket.org/HelgeOlav/fortigate v0.0.0-20220207164113-59794093808a // indirect
	bitbucket.org/HelgeOlav/utils v0.0.0-20211209222926-7771de6afdcd // indirect
	github.com/mitchellh/mapstructure v1.4.3 // indirect
)
