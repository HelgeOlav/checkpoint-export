package main

// This file contains what we need to decode from the cp_objects.json file

// https://stackoverflow.com/questions/47911187/golang-elegantly-json-decode-different-structures

import (
	checkpoint_export "bitbucket.org/HelgeOlav/checkpoint-export"
	"bitbucket.org/HelgeOlav/fortigate"
	"bitbucket.org/HelgeOlav/fortigate/zonemapper"
	"fmt"
	"net"
	"os"
)

func test2_printobjectnames(o checkpoint_export.ArrayTop, uids []string) {
	if len(uids) > 0 {
		for _, u := range uids {
			r := checkpoint_export.FindObjectByUID(&o, u)
			fmt.Printf("%s: %s\n", u, r["name"])
		}
	}
}

// Test of NewHost
func test1(o checkpoint_export.ArrayTop) {
	// find first value of kind
	for _, v := range o {
		if v["type"] == "access-layer" {
			// ok, process and leave
			h, err := checkpoint_export.NewAccessLayer(v)
			if err != nil {
				fmt.Println(err)
			} else {
				fmt.Println(h)
				//test2_printobjectnames(o, h.Members)
			}
			break
		}
	}
}

var testFile1 = "/Users/helge/go/src/bitbucket.org/HelgeOlav/checkpoint-export/debug/data.json"
var cpFile1 = "/Users/helge/temp/cp_objects.json"                   // contains address/services etc
var cpFile2 = "/Users/helge/temp/cp_Network-Management Server.json" // contains rules

func test4() {
	fmt.Println("helge")
	o1, _ := checkpoint_export.LoadObjectsFile(cpFile1)
	o, err := checkpoint_export.LoadObjectsFile(cpFile2)
	fmt.Println(err)
	fmt.Println(len(o))
	// fmt.Println(o[3]["type"])
	r := checkpoint_export.CountTypes(o)
	fmt.Println(r)
	// test1(o)
	fmt.Println(checkpoint_export.FindAllFirewallLayersFromRules(&o))
	test2(o, o1)
}

// test5 load address objects into Fortigate
func test5() {
	c, err := fortigate.DefaultConnection()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	c.IgnoreSSLCerts()
	o, _ := checkpoint_export.LoadObjectsFile(cpFile1)
	rules, _ := checkpoint_export.LoadObjectsFile(cpFile2)
	// CreateFgtAddressGroup(o)
	// CreateFgtServiceTCP(o)
	// CreateFgtServiceUDP(o)
	// CreateFgtServiceGroup(o)
	checkpoint_export.GaiaInterfaceMapper = getGaiaIfMap()
	gaiaRes, err := checkpoint_export.ParseGAIA(GaiaConfigFile)
	proc := checkpoint_export.FwPolicyProcessor{
		Objects:       o,
		ServiceMapper: checkpoint_export.NewDefaultServiceMapper(),
		NamesMapper:   checkpoint_export.NewDefaultAddressMapper(),
		ZoneMapper:    zonemapper.Routemapper{},
		LayersPath:    cpWorkDir,
	}
	//checkpoint_export.CreateFgtAddresses(c, proc.Objects, proc.NamesMapper)
	zm, err := zonemapper.LoadDefaultRoutemap()
	if err != nil {
		fmt.Println("Cannot load zone map:", err)
	} else {
		proc.ZoneMapper = *zm
	}
	res, err := proc.ProcessPolicies(rules)
	fmt.Println(err, res.LayerCounter, len(res.Policies))
	// print json
	//bytes, _ := json.Marshal(res.Policies)
	//fmt.Println(string(bytes))
	zones := proc.CreateZoneListFromInterface(gaiaRes.Interfaces)
	fmt.Println(zones)
}

// test6 works on ProcessPolicies to find out why each run differs.
func test6() {
	o, _ := checkpoint_export.LoadObjectsFile(cpFile1)
	rules, _ := checkpoint_export.LoadObjectsFile(cpFile2)
	proc := checkpoint_export.FwPolicyProcessor{
		Objects:       o,
		ServiceMapper: checkpoint_export.NewDefaultServiceMapper(),
		NamesMapper:   checkpoint_export.NewDefaultAddressMapper(),
		ZoneMapper:    zonemapper.Routemapper{},
		LayersPath:    cpWorkDir,
	}
	// load routemapper
	rm, _ := zonemapper.LoadFromFile("/Users/helge/CloudStation/Kripos/Checkpoint-migrering/zonemap.json")
	proc.ZoneMapper = *rm
	res1, err1 := proc.ProcessPolicies(rules)
	res2, err2 := proc.ProcessPolicies(rules)
	fmt.Println(err1, err2)
	fmt.Println("Loaded policies:", len(rules))
	fmt.Println("Created policies:", len(res1.Policies), len(res2.Policies))
}

func main() {
	test6()
}

func test2(rules, objects checkpoint_export.ArrayTop) {
	res, err := checkpoint_export.NewFirewallRule(rules[115])
	fmt.Println(err, res.Validate(), res)
	// print some info
	fmt.Println("Name:", res.Name, ", isLayer:", len(res.InlineLayer) > 0, ", enabled:", res.Enabled)
	fmt.Println("Source:")
	for _, v := range res.Source {
		o := checkpoint_export.FindObjectByUID(&objects, v)
		fmt.Println("", o["name"].(string), o["type"].(string))
	}
	fmt.Println("Destination:")
	for _, v := range res.Destination {
		o := checkpoint_export.FindObjectByUID(&objects, v)
		fmt.Println("", o["name"].(string), o["type"].(string))
	}
}

// func3 test of IP subnet from net package
func test3() {
	ip := net.ParseIP("192.168.101.1")
	ipmask := net.CIDRMask(22, 32)
	myNet := net.IPNet{IP: net.IPv4(192, 168, 100, 0), Mask: ipmask}
	fmt.Println(ip, ipmask, myNet)
	fmt.Println(myNet.Contains(ip))
}
