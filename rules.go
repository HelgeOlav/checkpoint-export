package checkpoint_export

import (
	"errors"
	"github.com/mitchellh/mapstructure"
)

// This file contains the definitions we need to handle from cp_Network-Management Server.json.
// Definitions from objects.go will be used when appropriate.

// AccessSelection, indicates a section of rules
type AccessSelection struct {
	UID  string
	Name string
	From int
	To   int
	Type string // is "access-selection"
}

// FirewallRule defines a firewall rule
// Deprecated: obsoleted by CpFwPolicy
type FirewallRule struct {
	Comment       string `mapstructure:"comments"`
	Color         string
	UID           string
	Name          string
	SourceNegate  bool `mapstructure:"source-negate"`
	ServiceNegate bool `mapstructure:"service-negate"`
	DestNegate    bool `mapstructure:"destination-negate"`
	Enabled       bool
	Destination   []string
	Source        []string
	Service       []string
	Content       []string
	Meta          MetaInfo `mapstructure:"meta-info"`
	RuleNumber    int      `mapstructure:"rule-number"`
	VPN           []string
	Domain        Domain
	Action        string
	Time          []string
	InstallOn     []string `mapstructure:"install-on"`
	InlineLayer   string   `mapstructure:"inline-layer"`
}

// Validate does some basic validation on a rule
func (f FirewallRule) Validate() error {
	// if we invert the rule
	if f.SourceNegate || f.DestNegate == true || f.ServiceNegate == true {
		return errors.New("some objects are negated")
	}
	// check if we have any source or destionation
	if len(f.Source) == 0 || len(f.Destination) == 0 {
		return errors.New("no source or destination object")
	}
	// check for service
	if len(f.Service) == 0 {
		return errors.New("no services")
	}
	return nil
}

// FirewallRule converts a map to object
func NewFirewallRule(input map[string]interface{}) (FirewallRule, error) {
	var h = new(FirewallRule)
	err := mapstructure.Decode(input, h)
	return *h, err
}

// FindAllFirewallLayersFromRules helper to give the index of all objects that references to inline layers
func FindAllFirewallLayersFromRules(o *ArrayTop) []int {
	res := []int{}
	if o != nil {
		for k, v := range *o {
			if v["type"] == "access-rule" {
				rule, err := NewFirewallRule(v)
				if err != nil {
					continue
				}
				if len(rule.InlineLayer) > 0 {
					res = append(res, k)
				}
			}
		}
	}
	return res
}
