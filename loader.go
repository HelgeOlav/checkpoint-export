package checkpoint_export

import (
	"encoding/json"
	"io/ioutil"
	"os"
)

// helper that loads JSON files into memory

// LoadObjectsFile This helper loads a JSON file into memory in the format of array as top level
func LoadObjectsFile(fileName string) (ArrayTop, error) {
	jsonFile, err := os.Open(fileName)
	if err != nil {
		return nil, err
	}
	defer jsonFile.Close()
	byteValue, _ := ioutil.ReadAll(jsonFile)
	// parse json
	var o ArrayTop
	err = json.Unmarshal(byteValue, &o)
	return o, err
}
