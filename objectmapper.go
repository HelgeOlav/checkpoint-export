package checkpoint_export

// MapperService Some names (services/groups/addresses) have different names for predefined objects and to reuse them you can
// use the mapper to rename objects.
// For services a service will not be created if it in the list. The service is expected to exist already.
type MapperService struct {
	Names      map[string]string // a list of old and new names
	NextMapper *MapperService    // you can chain to other mapper services
}

// Exist returns true if a name exists in a map, false otherwise.
// This check can be done to see if a object name needs to be added or not.
func (m *MapperService) Exist(input string) bool {
	if m != nil {
		if m.Names != nil {
			if _, ok := m.Names[input]; ok {
				return true
			}
			if m.NextMapper != nil {
				return m.NextMapper.Exist(input)
			}
		}
	}
	return false
}

// GetNames processes a list of names and returns a new list with names
func (m *MapperService) GetNames(input []string) (result []string) {
	for _, v := range input {
		result = append(result, m.GetName(v))
	}
	return
}

// GetName returns the translated name, if any or the original name if does not need to be translated.
// In case of errors it returns an empty string.
func (m *MapperService) GetName(input string) (result string) {
	if m != nil {
		if m.Names != nil {
			if k, ok := m.Names[input]; ok {
				return k
			}
			if m.NextMapper != nil {
				return m.NextMapper.GetName(input)
			}
			return input
		}
	}
	return ""
}

// NewDefaultAddressMapper returns a list of predefined mappings from Checkpoint to Fortigate.
func NewDefaultAddressMapper() MapperService {
	return MapperService{Names: map[string]string{"Any": "all", "ExternalZone": "all"}}
}

// NewDefaultServiceMapper returns a list of predefined mappings from Checkpoint to Fortigate.
func NewDefaultServiceMapper() MapperService {
	return MapperService{Names: map[string]string{
		"Any":     "ALL",
		"http":    "HTTP",
		"https":   "HTTPS",
		"dns":     "DNS",
		"smtp":    "SMTP",
		"ldap":    "LDAP",
		"RPC":     "DCE-RPC",
		"DNS_UDP": "DNS",
		"DNS_TCP": "DNS",
	}}
}
