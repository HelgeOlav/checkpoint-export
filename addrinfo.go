package checkpoint_export

import "bitbucket.org/HelgeOlav/fortigate"

// Outputs address information to file for debugging.

// MapOfAddresses struct that is returned with sone information about address analysis from MapAllAddressObjects.
type MapOfAddresses struct {
	CountHost    int                 `json:"count-host"`
	CountNetwork int                 `json:"count-network"`
	CountGroup   int                 `json:"count-group"`
	CountRange   int                 `json:"count-range"`
	Zones        map[string][]string `json:"zones,omitempty"`
	Objects      map[string][]string `json:"objects,omitempty"`
	Unmapped     []string            `json:"unmapped,omitempty"`
}

// MapAllAddressObjects does some analyses of objects to assist in troubleshooting a migration.
func (proc *FwPolicyProcessor) MapAllAddressObjects() MapOfAddresses {
	var result MapOfAddresses
	result.Zones = make(map[string][]string)
	result.Objects = make(map[string][]string)
	for _, objName := range proc.Objects {
		// find right object type
		t := objName["type"].(string)
		if t == "address-range" || t == "host" || t == "network" || t == "group" {
			n := proc.NamesMapper.GetName(objName["name"].(string))
			var ip []string
			switch t {
			case "host":
				ip = []string{objName["ipv4-address"].(string)}
				result.CountHost++
			case "network":
				result.CountNetwork++
				ip = []string{objName["subnet4"].(string)}
			case "group":
				result.CountGroup++
				uid := objName["uid"].(string)
				ip = proc.GetIPFromGUID([]string{uid})
			case "address-range":
				result.CountRange++
				ip = []string{objName["ipv4-address-first"].(string)}
			}
			// find zone for name
			for _, thisIp := range ip {
				if len(thisIp) > 0 {
					res := proc.ZoneMapper.FindZonesFromIP(thisIp)
					for _, v := range res {
						result.Zones[v] = append(result.Zones[v], n)
						result.Objects[n] = append(result.Objects[n], v)
					}
				} else {
					result.Unmapped = append(result.Unmapped, n)
				}
			}
		}
	}
	// remove duplicate objects and objects only in one zone
	for k, v := range result.Objects {
		res := fortigate.RemoveDuplicates(v)
		if len(res) > 1 {
			result.Objects[k] = res
		} else {
			delete(result.Objects, k)
		}
	}
	// remove duplicate zones
	for k, v := range result.Zones {
		result.Zones[k] = fortigate.RemoveDuplicates(v)
	}
	return result
}
