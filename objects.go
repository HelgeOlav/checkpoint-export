package checkpoint_export

import (
	"bitbucket.org/HelgeOlav/fortigate"
	"github.com/mitchellh/mapstructure"
)

// This file defines the format of the JSON file produced by exporting the configuration on R80.30

// This is a generic type to represent all JSON objects that is an array
type ArrayTop []map[string]interface{}

// Substruct domain, used in some object types
type Domain struct {
	UID  string
	Name string
	Type string `mapstructure:"domain-type"`
}

type Time struct {
	Posix uint64
	ISO   string `mapstructure:"iso-8601"`
}

// Substruct meta-info
type MetaInfo struct {
	Creator        string
	ValidState     string `mapstructure:"validation-state"`
	Created        Time   `mapstructure:"creation-time"`
	LastUpdated    Time   `mapstructure:"last-modify-time"`
	LastModifiedBy string `mapstructure:"last-modifier"`
}

// Checkpoint object of type host
type Host struct {
	Comment    string `mapstructure:"comments"`
	Color      string
	UID        string
	Name       string
	IPv4       string `mapstructure:"ipv4-address"`
	Domain     Domain
	Meta       MetaInfo `mapstructure:"meta-info"`
	Tags       []string
	Interfaces []string
}

// Checkpoint object of type address-range
type AddressRange struct {
	Comment string `mapstructure:"comments"`
	Color   string
	UID     string
	Name    string
	Meta    MetaInfo `mapstructure:"meta-info"`
	Tags    []string
	EndIP   string `mapstructure:"ipv4-address-last"`
	StartIP string `mapstructure:"ipv4-address-first"`
}

// Network is the Checkpoint object of type network
type Network struct {
	Comment    string `mapstructure:"comments"`
	Color      string
	UID        string
	Name       string
	Meta       MetaInfo `mapstructure:"meta-info"`
	Tags       []string
	Subnet4    string
	SubnetMask string `mapstructure:"subnet-mask"`
	Mask4      int    `mapstructure:"mask-length4"`
}

// ServiceTCP: Checkpoint object of type service-tcp
type ServiceTCP struct {
	Comment          string `mapstructure:"comments"`
	Timeout          int    `mapstructure:"session-timeout"`
	Color            string
	UID              string
	Name             string
	Meta             MetaInfo `mapstructure:"meta-info"`
	Tags             []string
	Protocol         string
	Port             string
	Domain           Domain
	MatchBySignature bool `mapstructure:"match-by-protocol-signature"`
}

// ServiceICMP Checkpoint object of type service-icmp
type ServiceICMP struct {
	Name     string
	UID      string
	Comment  string `mapstructure:"comments"`
	Color    string
	Meta     MetaInfo `mapstructure:"meta-info"`
	Tags     []string
	Domain   Domain
	IcmpType int `mapstructure:"icmp-type"`
}

// ServiceUDP Checkpoint object of type service-udp
type ServiceUDP struct {
	Comment          string `mapstructure:"comments"`
	Timeout          int    `mapstructure:"session-timeout"`
	Color            string
	UID              string
	Name             string
	Meta             MetaInfo `mapstructure:"meta-info"`
	Tags             []string
	Protocol         string
	Port             string
	Domain           Domain
	MatchBySignature bool `mapstructure:"match-by-protocol-signature"`
}

// ServiceGroup Checkpoint object of type service-group
type ServiceGroup struct {
	Comment  string `mapstructure:"comments"`
	Color    string
	UID      string
	Name     string
	Meta     MetaInfo `mapstructure:"meta-info"`
	Tags     []string
	Protocol string
	Port     string
	Domain   Domain
	Members  []string
}

// Group Checkpoint object of type group
type Group struct {
	Comment  string `mapstructure:"comments"`
	Color    string
	UID      string
	Name     string
	Meta     MetaInfo `mapstructure:"meta-info"`
	Tags     []string
	Protocol string
	Port     string
	Domain   Domain
	Members  []string
}

// AccessLayer Checkpoint object of type access-layer
type AccessLayer struct {
	Comment          string `mapstructure:"comments"`
	Color            string
	UID              string
	Name             string
	Meta             MetaInfo `mapstructure:"meta-info"`
	Tags             []string
	ImplictAction    string `mapstructure:"implict-cleanup-action"`
	Shared           bool
	ContentAwareness bool `mapstructure:"content-awareness"`
	Filtering        bool `mapstructure:"application-and-url-filtering"`
	Firewall         bool
	MobileAccess     bool   `mapstructure:"mobile-access"`
	Parent           string `mapstructure:"parent-layer"`
}

// CpFwPolicy is a firewall policy rule
type CpFwPolicy struct {
	Comment           string `mapstructure:"comments"`
	Color             string
	UID               string
	Name              string
	Meta              MetaInfo `mapstructure:"meta-info"`
	Destination       []string // source of rule
	Source            []string // destination of rule
	Service           []string
	Content           []string
	Action            string // accept, deny or other
	Enabled           bool
	SourceNegate      bool   `mapstructure:"source-negate"`
	DestinationNegate bool   `mapstructure:"destination-negate"`
	RuleNumber        int    `mapstructure:"rule-number"`
	InlineLayer       string `mapstructure:"inline-layer"`
	Domain            Domain
}

// NewHost helper func to convert map to Host
func NewHost(input map[string]interface{}) (Host, error) {
	var h = new(Host)
	//err := Map2Interface(input, h)
	err := mapstructure.Decode(input, h)
	return *h, err
}

// NewPolicy helper func to convert map to Policy
func NewPolicy(input map[string]interface{}) (CpFwPolicy, error) {
	var h = new(CpFwPolicy)
	err := mapstructure.Decode(input, h)
	return *h, err
}

// NewAddressRange Helper func to convert map to AddressRange
func NewAddressRange(input map[string]interface{}) (AddressRange, error) {
	var h = new(AddressRange)
	err := mapstructure.Decode(input, h)
	return *h, err
}

// NewNetwork Helper func to convert map to Network
func NewNetwork(input map[string]interface{}) (Network, error) {
	var h = new(Network)
	err := mapstructure.Decode(input, h)
	return *h, err
}

// NewServiceTCP func to convert map to ServiceTCP
func NewServiceTCP(input map[string]interface{}) (ServiceTCP, error) {
	var h = new(ServiceTCP)
	err := mapstructure.Decode(input, h)
	return *h, err
}

// NewServiceICMP func to convert map to ServiceICMP
func NewServiceICMP(input map[string]interface{}) (ServiceICMP, error) {
	var h = new(ServiceICMP)
	err := mapstructure.Decode(input, h)
	return *h, err
}

// NewServiceUDP func to convert map to ServiceUDP
func NewServiceUDP(input map[string]interface{}) (ServiceUDP, error) {
	var h = new(ServiceUDP)
	err := mapstructure.Decode(input, h)
	return *h, err
}

// NewServiceGroup func to convert map to ServiceGroup
func NewServiceGroup(input map[string]interface{}) (ServiceGroup, error) {
	var h = new(ServiceGroup)
	err := mapstructure.Decode(input, h)
	return *h, err
}

// NewGroup func to convert map to Group
func NewGroup(input map[string]interface{}) (Group, error) {
	var h = new(Group)
	err := mapstructure.Decode(input, h)
	return *h, err
}

// NewAccessLayer func to convert map to AccessLayer
func NewAccessLayer(input map[string]interface{}) (AccessLayer, error) {
	var h = new(AccessLayer)
	err := mapstructure.Decode(input, h)
	return *h, err
}

// CountTypes Helper method to count the number of different "type" objects in an array
func CountTypes(o ArrayTop) map[string]int {
	r := make(map[string]int)
	// loop through and count all types
	for _, v := range o {
		t := v["type"].(string)
		if len(t) > 0 {
			if _, ok := r[t]; ok {
				r[t] = r[t] + 1
			} else {
				r[t] = 1
			}
		}
	}
	return r
}

// FindObjectByUID searches through all objects to find the occurrence with the specified UID
func FindObjectByUID(o *ArrayTop, uid string) map[string]interface{} {
	if o != nil {
		// search through
		for _, v := range *o {
			if v["uid"] == uid {
				return v
			} // if
		} // for
	}
	// if not found, return empty map
	return make(map[string]interface{})
}

func (h Host) NewFortigateHost() fortigate.Addr {
	return fortigate.Addr{
		Name:    h.Name,
		Subnet:  h.IPv4 + "/32",
		Comment: h.Comment,
		Color:   10,
	}
}

func (n Network) NewFortigateHost() fortigate.Addr {
	return fortigate.Addr{
		Name:    n.Name,
		Subnet:  n.Subnet4 + " " + n.SubnetMask,
		Comment: n.Comment,
		Color:   10,
	}
}

// NewFortigateHost returns a new Fortigate address iprange object
func (r AddressRange) NewFortigateHost() fortigate.Addr {
	return fortigate.Addr{
		Name:    r.Name,
		Type:    fortigate.AddrTypeIpRange,
		StartIP: r.StartIP,
		EndIP:   r.EndIP,
		Comment: r.Comment,
		Color:   10,
	}
}

func (s ServiceICMP) NewFortigateService() (result fortigate.FirewallService) {
	result.Color = 10
	result.Name = s.Name
	result.Comment = s.Comment
	result.Protocol = fortigate.ServiceICMP
	result.IcmpType = s.IcmpType
	return
}

func (s ServiceTCP) NewFortigateService() (result fortigate.FirewallService) {
	result.Color = 10
	result.Name = s.Name
	result.Comment = s.Comment
	result.Protocol = fortigate.ServiceTCPUDP
	result.TcpPortRange = s.Port
	return result
}

func (s ServiceUDP) NewFortigateService() (result fortigate.FirewallService) {
	result.Color = 10
	result.Name = s.Name
	result.Protocol = fortigate.ServiceTCPUDP
	result.UdpPortRange = s.Port
	result.Comment = s.Comment
	return result
}
