package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
)

// Handle command line

var outputDebugDir, cmdGaia, cmdObj, cmdRule, cmdFg, cmdMap, cmdZone, cmdLay *string
var outputCrt, outputCa, outputCag, outputCs, outputCsg, outputCi, outputCz, outputCr *bool
var outputIfUp *bool
var outputIfAllowAccess *string
var outputPolicyLog *string
var outputAdjIfIp *int
var outputIfNamePrefix, outputIfNameSuffix *string
var outputOptimizeRule *bool

// CmdLineOpts reads a JSON file and uses those options as defaults that can be overridden.
// Names matches command line options
type CmdLineOpts struct {
	Gaia                         string
	Obj                          string
	Rule                         string
	Fg                           string
	Map                          string
	Zone                         string
	Lay                          string
	IfUp                         bool
	DebugDir                     string
	AdjIfIp                      int
	Ca, Cag, Cs, Csg, Ci, Cz, Cr bool
	Crt                          bool   // create route
	ORule                        bool   // optimize rule source/dest
	IfAllowAccess                string // set of allowed accesses, like PING
	IfNameSuffix                 string // add at the end of interface names
	IfNamePrefix                 string // add to the start of the interface names
}

func loadOpts() CmdLineOpts {
	var result CmdLineOpts
	env := os.Getenv("CP2FG")
	if len(env) == 0 {
		env = "cp2fg.json"
	}
	// try load file
	b, err := os.ReadFile(env)
	if err != nil {
		fmt.Println(err)
	} else {
		_ = json.Unmarshal(b, &result)
	}
	return result
}
func cmdLine() {
	defaultOps := loadOpts()
	// specify parameters
	cmdGaia = flag.String("gaia", defaultOps.Gaia, "GAIA configuration")
	cmdObj = flag.String("obj", defaultOps.Obj, "CP object file")
	cmdRule = flag.String("rule", defaultOps.Rule, "CP rule file")
	cmdFg = flag.String("fg", defaultOps.Fg, "Fortigate connection file")
	cmdMap = flag.String("map", defaultOps.Map, "Mapping of interface names")
	cmdZone = flag.String("zone", defaultOps.Zone, "Mapping of IP to zones")
	cmdLay = flag.String("lay", defaultOps.Lay, "Path to Checkpoint export directory (optional)")
	outputCa = flag.Bool("ca", defaultOps.Ca, "Create address objects")
	outputCag = flag.Bool("cag", defaultOps.Cag, "Create address groups")
	outputCs = flag.Bool("cs", defaultOps.Cs, "Create service objects")
	outputCsg = flag.Bool("csg", defaultOps.Csg, "Create service groups")
	outputCi = flag.Bool("ci", defaultOps.Ci, "Create interfaces")
	outputCz = flag.Bool("cz", defaultOps.Cz, "Create firewall zones")
	outputCr = flag.Bool("cr", defaultOps.Cr, "Create firewall rules")
	outputCrt = flag.Bool("crt", defaultOps.Crt, "Create firewall routes")
	outputAll := flag.Bool("all", false, "Turn on all output")
	outputIfUp = flag.Bool("ifup", defaultOps.IfUp, "Set interface up/down status")
	outputAdjIfIp = flag.Int("adjifip", defaultOps.AdjIfIp, "Adjust interface IP on output, can be negative number")
	outputPolicyLog = flag.String("logtype", "all", "Type of firewall policy logging")
	outputDebugDir = flag.String("debugdir", defaultOps.DebugDir, "Directory to save some debug info to")
	outputOptimizeRule = flag.Bool("orule", defaultOps.ORule, "optimize rule source/dest")
	outputIfAllowAccess = flag.String("ifallowaccess", defaultOps.IfAllowAccess, "services allowed on interface")
	outputIfNamePrefix = flag.String("ifnameprefix", defaultOps.IfNamePrefix, "prefix to generated IF name")
	outputIfNameSuffix = flag.String("ifnamesuffix", defaultOps.IfNameSuffix, "suffix to generated IF name")
	// parse
	flag.Parse()
	// check for errors, and print syntax if needed.
	if len(*cmdGaia) == 0 || len(*cmdObj) == 0 || len(*cmdRule) == 0 || len(*cmdZone) == 0 || len(*cmdMap) == 0 {
		flag.PrintDefaults()
		if len(*cmdGaia) == 0 {
			fmt.Println("Missing -gaia")
		}
		if len(*cmdObj) == 0 {
			fmt.Println("Missing -obj")
		}
		if len(*cmdRule) == 0 {
			fmt.Println("Missing -rule")
		}
		if len(*cmdZone) == 0 {
			fmt.Println("Missing -zone")
		}
		if len(*cmdMap) == 0 {
			fmt.Println("Missing -map")
		}
		os.Exit(1)
	}
	if *outputAll {
		t := true
		outputCa = &t
		outputCag = &t
		outputCs = &t
		outputCsg = &t
		outputCi = &t
		outputCz = &t
		outputCr = &t
	}
}
