package main

import "testing"

func Test_adjustIP(t *testing.T) {
	// test 1 - add 1 on IPv4
	s := "192.168.0.1/24"
	res := AdjustIP(s, 1)
	if res != "192.168.0.2/24" {
		t.Errorf("AdjustIP expected 192.168.0.2/24, got %s", res)
	}
	// test 2 - substract 1 on IPv4
	s = "192.168.0.2/24"
	res = AdjustIP(s, -1)
	if res != "192.168.0.1/24" {
		t.Errorf("AdjustIP expected 192.168.0.1/24, got %s", res)
	}
	// test 3 - substract 1 on IPv6
	s = "fe80::d06e:bdff:fe35:2/64"
	res = AdjustIP(s, -1)
	if res != "fe80::d06e:bdff:fe35:1/64" {
		t.Errorf("AdjustIP expected fe80::d06e:bdff:fe35:1/64, got %s", res)
	}

}
