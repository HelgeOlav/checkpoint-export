package checkpoint_export

import (
	"bitbucket.org/HelgeOlav/fortigate"
	"bufio"
	"fmt"
	"net"
	"os"
	"regexp"
	"strconv"
	"strings"
)

// Parser for GAIA configuration.
// This parser will create relevant Fortigate objects.
// Interfaces with IP addresses will be parsed, with DHCP and VLAN tags on subinterfaces.
// Only interfaces that have a source -> dest mapping will be translated.
// Static routes will be parsed.

// GaiaResult is the result from parsing the configuration.
type GaiaResult struct {
	LineCount int
	// Routes map of static routes to add. The key value is the name from GAIA, and inside StaticRoute the
	// Fortigate name is used.
	Routes map[string]fortigate.StaticRoute
	// Interfaces map of interfaces read from GAIA. The map key is the name from GAIA, the Interface name is the new on Fortigate.
	Interfaces map[string]fortigate.Interface
}

// ParseGAIA main function to parse the config file.
func ParseGAIA(input string) (*GaiaResult, error) {
	// open file
	file, err := os.Open(input)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	// Create result
	result := new(GaiaResult)
	result.Interfaces = make(map[string]fortigate.Interface)
	result.Routes = make(map[string]fortigate.StaticRoute)
	// read the file line by line
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		result.LineCount++
		line := scanner.Text()
		parseInterfaces(result, line)
		parseRoutes(result, line)
	}
	// and we are done
	result.UpdateStaticRoutes()
	return result, nil
}

var staticRouteNextHopRegex = regexp.MustCompile("^set static-route (.*) nexthop gateway address (.*) on$")
var staticRouteCommectRegex = regexp.MustCompile("^set static-route (.+) comment \\\"(.+)\\\".*$")

func parseRoutes(result *GaiaResult, input string) {
	if strings.HasPrefix(input, "set static-route ") {
		// check for next-hop
		if staticRouteNextHopRegex.MatchString(input) {
			m := staticRouteNextHopRegex.FindStringSubmatch(input)
			var output fortigate.StaticRoute
			prefix := m[1]
			nexthop := m[2]
			if _, ok := result.Routes[prefix]; ok {
				output = result.Routes[prefix]
			}
			output.Gateway = nexthop
			output.Distance = 10
			if prefix == "default" {
				output.Dst = "0.0.0.0 0.0.0.0"
			} else {
				output.Dst = prefix
			}
			result.Routes[prefix] = output
			return
		} // if NextHopRegex
		if staticRouteCommectRegex.MatchString(input) {
			m := staticRouteCommectRegex.FindStringSubmatch(input)
			prefix := m[1]
			comment := m[2]
			// at this point we assume that the static route already exist, as gateway appears above comment in config
			output := result.Routes[prefix]
			output.Comment = comment
			result.Routes[prefix] = output
		}
	}
}

// MakeSureInterfaceExists helper that creates an empty key in the map if one does not exist
func (g *GaiaResult) MakeSureInterfaceExists(i string) {
	if _, ok := g.Interfaces[i]; !ok {
		g.Interfaces[i] = fortigate.Interface{}
	}
}

// UpdateStaticRoutes has to be called at the end to map a static route to an interface
func (g *GaiaResult) UpdateStaticRoutes() {
	for k, v := range g.Routes { // outer loop, go through and update each route
		ip := net.ParseIP(v.Gateway)
		for _, intf := range g.Interfaces { // inner loop, search through interfaces
			_, prefix, _ := net.ParseCIDR(intf.IP)
			if prefix != nil {
				if prefix.Contains(ip) { // we have a match
					v.Device = intf.Name
					g.Routes[k] = v
					break
				}
			}
		}
		if len(v.Device) == 0 {
			fmt.Println("Route did not find its outgoing interface", v)
		}
	}
}

var gaiaParseIfNameAndVlan = regexp.MustCompile("(\\w+)\\.(\\d+)")

// GaiaInterfaceMapper maps Checkpoint to Fortigate. Key is physical port on Checkpoint, value is mapped port.
var GaiaInterfaceMapper map[string]string

// gaiaRenameInterface returns new interface, or blank if not mapped during migration.
// Input is name in form of "name[.vlan]".
// If newName is blank, then there is no mapping
func gaiaRenameInterface(input string) (newName, physPort string, vlan int) {
	// check if we have a name and VLAN
	var tmpPhysPort string
	if gaiaParseIfNameAndVlan.MatchString(input) {
		m := gaiaParseIfNameAndVlan.FindStringSubmatch(input)
		vlan, _ = strconv.Atoi(m[2])
		tmpPhysPort = m[1]
	} else {
		tmpPhysPort = input
	}
	// now see if the interface name is listed in our list
	if _, ok := GaiaInterfaceMapper[tmpPhysPort]; !ok {
		return
	}
	physPort = GaiaInterfaceMapper[tmpPhysPort]
	// if mapped, set newName as either VLAN### or physPort
	if vlan > 0 {
		newName = "VLAN" + strconv.Itoa(vlan)
	} else {
		newName = physPort
	}
	return
}

var intfComment = regexp.MustCompile("^set interface (.*) comments \"(.*)\"")
var intfIpAddr = regexp.MustCompile("^set interface (.+) ipv4-address (.+) mask-length (\\d*)")
var intfRelay = regexp.MustCompile("^set bootp interface (.+) relay-to (.+) on")

func parseInterfaces(result *GaiaResult, input string) {
	// comments, should come after the interface is first defined
	if intfComment.MatchString(input) {
		m := intfComment.FindStringSubmatch(input)
		nn, _, _ := gaiaRenameInterface(m[1])
		if len(nn) == 0 {
			return
		}
		i := result.Interfaces[m[1]]
		i.Description = m[2]
		result.Interfaces[m[1]] = i
	} else
	// ip address
	if intfIpAddr.MatchString(input) {
		m := intfIpAddr.FindStringSubmatch(input)
		nn, pp, vlan := gaiaRenameInterface(m[1])
		if len(nn) == 0 {
			return
		}
		result.MakeSureInterfaceExists(m[1])
		i := result.Interfaces[m[1]]
		i.Name = nn
		i.IP = m[2] + "/" + m[3]
		i.VDOM = "root" //TODO: remove hardcoding of VDOM reference
		i.Color = 10
		if vlan > 0 {
			i.Interface = pp
			i.VlanID = vlan
			i.Type = "vlan"
			i.VlanProtocol = "8021q"
		}
		result.Interfaces[m[1]] = i
	} else
	// DHCP relay - we assume that interface map is already created at this point
	if intfRelay.MatchString(input) {
		m := intfRelay.FindStringSubmatch(input)
		i := result.Interfaces[m[1]]
		if len(i.DhcpRelayIP) == 0 {
			i.DhcpRelayType = "regular"
			i.DhcpRelayService = "enable"
			i.DhcpRelayIP = "\"" + m[2] + "\" "
		} else {
			i.DhcpRelayIP = i.DhcpRelayIP + "\"" + m[2] + "\" "
		}
		result.Interfaces[m[1]] = i
	}
}
