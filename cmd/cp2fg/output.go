package main

import (
	checkpoint_export "bitbucket.org/HelgeOlav/checkpoint-export"
	"bitbucket.org/HelgeOlav/fortigate"
	"bitbucket.org/HelgeOlav/utils"
	"encoding/json"
	"fmt"
	"log"
	"net"
	"os"
	"strconv"
	"strings"
)

// saveJsonToFile save object to file, causing no error if any error.
// It is saved to the directory in -debugdir
func saveJsonToFile(fn string, o interface{}) {
	if len(*outputDebugDir) == 0 {
		return
	}
	of := *outputDebugDir + string(os.PathSeparator) + fn
	if o != nil {
		b, err := json.MarshalIndent(o, "", " ")
		if err != nil {
			fmt.Println("JSON marshal of", fn, err)
		} else {
			f, err := os.Create(of)
			if err != nil {
				fmt.Println("JSON save", err)
			} else {
				_, _ = f.Write(b)
				_ = f.Close()
			}
		}
	}
}

func createOutput() {
	var err error
	if *outputCa {
		checkpoint_export.CreateFgtAddresses(fgConnection, processor.Objects, processor.NamesMapper)
	}
	if *outputCag {
		checkpoint_export.CreateFgtAddressGroup(fgConnection, processor.Objects, processor.NamesMapper)
	}
	if *outputCs {
		checkpoint_export.CreateFgtServiceTCP(fgConnection, processor.Objects, processor.ServiceMapper)
		checkpoint_export.CreateFgtServiceUDP(fgConnection, processor.Objects, processor.ServiceMapper)
		checkpoint_export.CreateFgtServiceICMP(fgConnection, processor.Objects, processor.ServiceMapper)
	}
	if *outputCsg {
		checkpoint_export.CreateFgtServiceGroup(fgConnection, processor.Objects, processor.ServiceMapper)
	}
	// parse GAIA
	var gaiaRes *checkpoint_export.GaiaResult
	if *outputCi || *outputCz || *outputCrt {
		gaiaRes, err = checkpoint_export.ParseGAIA(*cmdGaia)
		if err != nil {
			log.Println("GAIA parsing failed:", err)
		} else {
			log.Println("Gaia configuration contained", len(gaiaRes.Interfaces), "interfaces and", len(gaiaRes.Routes), "routes.")
			// change IP address
			if *outputAdjIfIp != 0 {
				for k, v := range gaiaRes.Interfaces {
					if len(v.IP) > 0 {
						v.IP = AdjustIP(v.IP, *outputAdjIfIp)
						gaiaRes.Interfaces[k] = v
					}
				}
			}
			// add if-name - prefix and suffix
			if len(*outputIfNameSuffix) > 0 || len(*outputIfNamePrefix) > 0 {
				// look through interfaces
				for k, v := range gaiaRes.Interfaces {
					if len(v.Interface) > 0 { // only if not physical interface
						v.Name = *outputIfNamePrefix + v.Name + *outputIfNameSuffix
						gaiaRes.Interfaces[k] = v
					}
				}
				// look through routes
				for k, v := range gaiaRes.Routes {
					if strings.HasPrefix(v.Device, "VLAN") { // if VLAN interface
						v.Device = *outputIfNamePrefix + v.Device + *outputIfNameSuffix
						gaiaRes.Routes[k] = v
					}
				}
			}
			saveJsonToFile("gaia-parsed.json", gaiaRes)
		}
		if *outputCi {
			p := fortigate.NewParam()
			// ok, lets create the interfaces
			for k, ifEntry := range gaiaRes.Interfaces {
				// check if valid interface
				if len(ifEntry.IP) == 0 || len(ifEntry.VDOM) == 0 {
					log.Println("Not creating interface", k) // using CP name in case of invalid name conversion
					continue
				}
				if utils.IsStringInStrings(ifEntry.Name, &processor.SkipInterfaces) {
					continue
				}
				// set up status
				if *outputIfUp {
					ifEntry.Status = "up"
				} else {
					ifEntry.Status = "down"
				}
				// set misc properties
				ifEntry.AllowAccess = *outputIfAllowAccess
				ifEntry.VDOM = fgConnection.VDOM
				// and create it
				outName := "if-" + ifEntry.Name + ".json"
				saveJsonToFile(outName, ifEntry)
				var err error
				var res fortigate.FortigateResult
				// create subinterface and update physical interface
				if len(ifEntry.Interface) > 0 {
					res, err = fgConnection.CreateFortiObject(p, ifEntry)
				} else {
					res, err = fgConnection.UpdateFortiObject(p, ifEntry)
				}
				if err != nil {
					log.Println("-ci failed on", k, "-", err, res.CLIError)
				}
			}
		}
	}
	if *outputCz {
		if gaiaRes != nil {
			p := fortigate.NewParam()
			zones := processor.CreateZoneListFromInterface(gaiaRes.Interfaces)
			fmt.Println("Zones mapped:")
			for k, v := range zones {
				fmt.Printf(" %s: %d interfaces\n", k, len(v))
			}
			saveJsonToFile("zones.json", zones)
			// create zones
			for k, v := range zones {
				zone := fortigate.Zone{
					Name:      k,
					Intrazone: "deny",
				}
				zone.SetInterfaces(v)
				of := fmt.Sprintf("zones-%s.json", k)
				saveJsonToFile(of, zone)
				res, err := fgConnection.CreateFortiObject(p, zone)
				if err != nil {
					fmt.Println("Zone", k, "failed", err, res.CLIError)
				}
			}
		} else {
			log.Println("-cz failed on dependency with GAIA, see line above.")
		}
	}
	if *outputCr {
		// dump address info
		addrMap := processor.MapAllAddressObjects()
		saveJsonToFile("addr-to-zone.json", addrMap)
		// process rules
		processor.ORule = *outputOptimizeRule
		res, err := processor.ProcessPolicies(topRules)
		if err != nil {
			log.Println("Rules processor exited with error", err)
		} else {
			log.Printf("Rules processor processed %d rules(created %d rules) in top and %d layers, and had %d errors.\n", res.RulesCounter, len(res.Policies), res.LayerCounter, res.Errors)
			saveJsonToFile("rules.json", res)
			p := fortigate.NewParam()
			var countOk, countError int
			for k, v := range res.Policies {
				fn := "rules-" + strconv.Itoa(k) + ".json"
				v.LogTraffic = *outputPolicyLog // set log type
				saveJsonToFile(fn, v)
				// create policy
				createRes, err := fgConnection.CreateFortiObject(p, v)
				if err != nil {
					countError++
					fmt.Println("Error creating policy", k, "-", v.Name, "-", err)
					fn := "rules-" + strconv.Itoa(k) + "-error.json"
					saveJsonToFile(fn, createRes)
				} else {
					countOk++
				}
			}
			log.Println("Fortigate created", countOk, "policies, and", countError, "errors.")
		}
	}
	// create static routes
	if *outputCrt {
		p := fortigate.NewParam()
		for k, v := range gaiaRes.Routes {
			if !utils.IsStringInStrings(v.Device, &processor.SkipInterfaces) {
				res, err := fgConnection.CreateFortiObject(p, v)
				if err != nil {
					log.Println("Static route", k, err)
					saveJsonToFile("staticroute-error"+k+".json", res)
				}
			}
		}
	}
}

// AdjustIP subtracts or adds to an IP address in CIDR format
func AdjustIP(input string, adj int) string {
	if adj == 0 || len(input) < 8 {
		return input
	}
	i, n, err := net.ParseCIDR(input)
	if err != nil || n == nil {
		log.Println("AdjustIP:", input, err)
		return input
	}
	l := len(i) - 1
	i[l] = i[l] + byte(adj)
	s, _ := n.Mask.Size()
	return i.String() + "/" + strconv.Itoa(s)
}
