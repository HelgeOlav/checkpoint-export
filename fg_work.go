package checkpoint_export

import (
	"bitbucket.org/HelgeOlav/fortigate"
	"encoding/json"
	"log"
)

// Work with the Fortigate

func CreateFgtAddresses(c fortigate.Connection, o ArrayTop, serviceMapper MapperService) {
	var errorCounter int = 0
	// loop through
	for _, v := range o {
		if v["type"] == "host" && serviceMapper.Exist(v["name"].(string)) == false {
			h, err := NewHost(v)
			if err == nil {
				fgHost := h.NewFortigateHost()
				err := c.CreateAddressObject(fgHost)
				if err != nil {
					log.Println(err, fgHost)
					errorCounter++
					if errorCounter > 3 {
						bodyBytes, _ := json.Marshal(fgHost)
						log.Println(string(bodyBytes))
						//os.Exit(1)
					}
				}
			} else {
				log.Println(v["name"], err)
			}
		} else if v["type"] == "address-range" && serviceMapper.Exist(v["name"].(string)) == false {
			h, err := NewAddressRange(v)
			if err == nil {
				fgHost := h.NewFortigateHost()
				err := c.CreateAddressObject(fgHost)
				if err != nil {
					log.Println(err, fgHost)
					errorCounter++
					if errorCounter > 3 {
						bodyBytes, _ := json.Marshal(fgHost)
						log.Println(string(bodyBytes))
					}
				}
			} else {
				log.Println(v["name"], err)
			}
		} else if v["type"] == "network" && serviceMapper.Exist(v["name"].(string)) == false {
			h, err := NewNetwork(v)
			if err == nil {
				fgHost := h.NewFortigateHost()
				err := c.CreateAddressObject(fgHost)
				if err != nil {
					log.Println(err, fgHost)
					errorCounter++
					if errorCounter > 3 {
						bodyBytes, _ := json.Marshal(fgHost)
						log.Println(string(bodyBytes))
						//os.Exit(1)
					}
				}
			} else {
				log.Println(v["name"], err)
			}
		}
	}
	log.Println("CreateFgtAddresses exited with", errorCounter, "errors.")
}

func CreateFgtAddressGroup(c fortigate.Connection, o ArrayTop, serviceMapper MapperService) {
	var errorCounter int = 0
	// loop through
	for _, v := range o {
		if v["type"] == "group" && serviceMapper.Exist(v["name"].(string)) == false {
			h, err := NewGroup(v)
			if err == nil {
				fg := fortigate.AddrGrp{
					Name:    h.Name,
					Comment: h.Comment,
					Color:   10,
				}
				// add group members
				var members []string
				for _, member := range h.Members {
					m := FindObjectByUID(&o, member)
					members = append(members, serviceMapper.GetName(m["name"].(string)))
				}
				fg.Member = fortigate.NewLinkedObjRefFromStrings(members)
				// create group
				err := c.CreateAddressGroupObject(fg)
				if err != nil {
					bodyBytes, _ := json.Marshal(fg)
					log.Println("Create group failed: ", err, string(bodyBytes))
					errorCounter++
				}
			} else {
				log.Println("Read error: ", v["name"], err)
			}
		}
	}
	log.Println("CreateFgtAddressGroup exited with", errorCounter, "errors.")
}

func CreateFgtServiceTCP(c fortigate.Connection, o ArrayTop, serviceMapper MapperService) {
	p := fortigate.NewParam()
	var errorCounter int = 0
	// loop through
	for _, v := range o {
		if v["type"] == "service-tcp" && serviceMapper.Exist(v["name"].(string)) == false {
			h, err := NewServiceTCP(v)
			if err == nil {
				fg := h.NewFortigateService()
				res, err := c.CreateFortiObject(p, fg)
				if err != nil {
					bodyBytes, _ := json.Marshal(fg)
					log.Println("Create TCP service failed: ", err, res.Status)
					log.Println(string(bodyBytes))
					errorCounter++
				}
			} else {
				errorCounter++
				log.Println("Read error: ", v["name"], err)
			}
		}
	}
	log.Println("CreateFgtServiceTCP exited with", errorCounter, "errors.")
}

func CreateFgtServiceUDP(c fortigate.Connection, o ArrayTop, serviceMapper MapperService) {
	p := fortigate.NewParam()
	var errorCounter int = 0
	// loop through
	for _, v := range o {
		if v["type"] == "service-udp" && serviceMapper.Exist(v["name"].(string)) == false {
			h, err := NewServiceUDP(v)
			if err == nil {
				fg := h.NewFortigateService()
				res, err := c.CreateFortiObject(p, fg)
				if err != nil {
					bodyBytes, _ := json.Marshal(fg)
					log.Println("Create UDP service failed: ", err, res.Status)
					log.Println(string(bodyBytes))
					errorCounter++
				}
			} else {
				log.Println("Read error: ", v["name"], err)
				errorCounter++
			}
		}
	}
	log.Println("CreateFgtServiceUDP exited with", errorCounter, "errors.")
}

func CreateFgtServiceGroup(c fortigate.Connection, o ArrayTop, serviceMapper MapperService) {
	p := fortigate.NewParam()
	var errorCounter int = 0
	// loop through
	for _, v := range o {
		if v["type"] == "service-group" && serviceMapper.Exist(v["name"].(string)) == false {
			h, err := NewServiceGroup(v)
			if err == nil {
				fg := fortigate.FirewallServiceGroup{
					Name:    h.Name,
					Comment: h.Comment,
					Color:   10,
				}
				// add group members
				var members []string
				for _, member := range h.Members {
					m := FindObjectByUID(&o, member)
					members = append(members, serviceMapper.GetName(m["name"].(string)))
				}
				fg.Member = fortigate.NewLinkedObjRefFromStrings(members)
				// create group
				res, err := c.CreateFortiObject(p, fg)
				if err != nil {
					bodyBytes, _ := json.Marshal(fg)
					log.Println("Create service group failed: ", err, res.Status)
					log.Println(string(bodyBytes))
					errorCounter++
				}
			} else {
				errorCounter++
				log.Println("Read error: ", v["name"], err)
			}
		}
	}
	log.Println("CreateFgtServiceGroup exited with", errorCounter, "errors.")
}

func CreateFgtServiceICMP(c fortigate.Connection, o ArrayTop, serviceMapper MapperService) {
	p := fortigate.NewParam()
	var errorCounter int = 0
	// loop through
	for _, v := range o {
		if v["type"] == "service-icmp" && serviceMapper.Exist(v["name"].(string)) == false {
			h, err := NewServiceICMP(v)
			if err == nil {
				fg := h.NewFortigateService()
				res, err := c.CreateFortiObject(p, fg)
				if err != nil {
					bodyBytes, _ := json.Marshal(fg)
					log.Println("Create TCP service failed: ", err, res.Status)
					log.Println(string(bodyBytes))
					errorCounter++
				}
			} else {
				errorCounter++
				log.Println("Read error: ", v["name"], err)
			}
		}
	}
	log.Println("CreateFgtServiceICMP exited with", errorCounter, "errors.")
}
