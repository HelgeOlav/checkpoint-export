package checkpoint_export

import (
	"bitbucket.org/HelgeOlav/fortigate"
	"bitbucket.org/HelgeOlav/fortigate/zonemapper"
	"bitbucket.org/HelgeOlav/utils"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

// Handles firewall policies

type FwPolicyResult struct {
	RulesCounter         int                // number of rules processed
	RulesDisabledCounter int                // number of disabled rules
	NonRulesCounter      int                // number of entries that are not rules
	LayerCounter         int                // number of layers processed
	Errors               int                // number of errors encountered
	Policies             []fortigate.Policy // the resulting firewall rules

}

// FwPolicyProcessor is the frontend processor for handling firewall rule migration.
// The processor keeps all the structs we need to convert the rules successfully.
type FwPolicyProcessor struct {
	ServiceMapper      MapperService          // services that we remap
	NamesMapper        MapperService          // hosts / networks that we remap
	ZoneMapper         zonemapper.Routemapper // map hosts to zones
	Objects            ArrayTop               // our Checkpoint objects
	CpmiAnyObject      string                 // UID of this object that is used in many places
	CpmiAllObject      string                 // UID of this object that is used in many places
	AcceptObject       string                 // UID of the accept rule object
	ExternalZoneObject string                 // UID of external zone
	LayersPath         string                 // path to directory with firewall layers files
	UniqueFwNameMap    map[string]int         // used to keep track of how many times similar rule names have been used
	NATRules           []string               // rules that should be interface NAT'ed (based on rule name)
	NATPool            map[string]string      // rules that will be NAT'ed using a pool instead of interface
	SkipRules          []string               // rules that should be skipped (based on rule name)
	SkipInterfaces     []string               // interfaces to skip during export (for zones, routes and interface) - use output interface name
	ORule              bool                   // if true rules will be optimized (source/dest filtered based on zone)
}

const MaxPolicyNameLength = 30

// init Do some init stuff and set up some variables
func (proc *FwPolicyProcessor) init() {
	proc.UniqueFwNameMap = make(map[string]int)
	for _, v := range proc.Objects {
		switch v["name"].(string) {
		case "All":
			proc.CpmiAllObject = v["uid"].(string)
		case "Any":
			proc.CpmiAnyObject = v["uid"].(string)
		case "Accept":
			proc.AcceptObject = v["uid"].(string)
		case "ExternalZone":
			proc.ExternalZoneObject = v["uid"].(string)
		}
	}
}

// ProcessPolicies is the main loop for creating policies.
func (proc *FwPolicyProcessor) ProcessPolicies(o ArrayTop) (FwPolicyResult, error) {
	proc.init()
	var err error
	result := FwPolicyResult{}
	// loop through the content
	for _, v := range o {
		if v["type"] == "access-rule" {
			pol, err := NewPolicy(v)
			if err != nil {
				result.Errors++
			} else if pol.Enabled == false {
				result.RulesDisabledCounter++
			} else if len(pol.InlineLayer) > 0 && proc.SkipRule(pol.Name) == false {
				// TODO: perhaps remove this section of code as processing of nested sublayers is done in getFgPolicyRulesfromPolicy
				result.LayerCounter++
				layObj := FindObjectByUID(&proc.Objects, pol.InlineLayer)
				layName := layObj["name"].(string)
				log.Println("(todo) Loading layer", layName)
				layFile, err := findFileInDir(proc.LayersPath, layName, "json")
				if err == nil {
					layRules, err := LoadObjectsFile(layFile)
					if err == nil {
						for _, layRule := range layRules {
							cpLayRule, _ := NewPolicy(layRule)
							rules := proc.getFgPolicyRulesfromPolicy(cpLayRule, &pol)
							result.Policies = append(result.Policies, rules...)
							result.RulesCounter++
						}
					} else {
						log.Println("ProcessPolicies: error loading layer:", err)
					}
				} else {
					log.Println("ProcessPolicies: error finding layers:", err)
				}
			} else {
				rules := proc.getFgPolicyRulesfromPolicy(pol, nil)
				result.Policies = append(result.Policies, rules...)
				result.RulesCounter++
			}
		} else { // if access-rule
			result.NonRulesCounter++
		}
	}
	return result, err
}

// findFileInDir tries to find a file inside a directory, starting with name and ending with extension.
func findFileInDir(dir, name, extension string) (string, error) {
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		return "", err
	}
	for _, v := range files {
		f := v.Name()
		if strings.HasPrefix(f, name) && strings.HasSuffix(f, extension) {
			return dir + "/" + f, nil
		}
	}
	return "", errors.New("Could not locate file " + name)
}

// GetObjectNameFromGUID looks up all object identifiers and returns a list of names
func (proc *FwPolicyProcessor) GetObjectNameFromGUID(input []string) (result []string) {
	for _, v := range input {
		o := FindObjectByUID(&proc.Objects, v)
		result = append(result, o["name"].(string))
	}
	return
}

// GetIPFromGUID returns a list of IP addresses defined by the network object in the input list
func (proc *FwPolicyProcessor) GetIPFromGUID(input []string) (result []string) {
	for _, v := range input {
		o := FindObjectByUID(&proc.Objects, v)
		t := o["type"].(string)
		switch t {
		case "CpmiAnyObject":
			result = append(result, "0.0.0.0/0")
		case "security-zone":
			if o["uid"].(string) == proc.ExternalZoneObject {
				result = append(result, "0.0.0.0/0")
			} else {
				fmt.Println("GetIPFromGUID found unknown zone", o["uid"].(string))
			}
		case "CpmiClusterMember", "host", "CpmiGatewayCluster":
			result = append(result, o["ipv4-address"].(string))
		case "network":
			n, _ := NewNetwork(o)
			result = append(result, n.Subnet4+"/"+strconv.Itoa(n.Mask4))
		case "group":
			g, _ := NewGroup(o)
			newResult := proc.GetIPFromGUID(g.Members)
			result = append(result, newResult...)
		case "address-range":
			r, _ := NewAddressRange(o)
			result = append(result, r.StartIP)
		default:
			fmt.Println("GetIPFromGUID got unsupported type", t, "- ignoring")
		}
	}
	return
}

// getFgPolicyRulesfromPolicy is the internal workhorse for creating Fortigate policies from Checkpoint.
// From the input a set of policies are created. If it is a layered policy (parent != nil), it will change the policy
// source, destination and service it is any to the parent definition
func (proc *FwPolicyProcessor) getFgPolicyRulesfromPolicy(policy CpFwPolicy, parent *CpFwPolicy) []fortigate.Policy {
	result := []fortigate.Policy{}
	// exist if rule is disabled
	if policy.Enabled == false {
		return result
	}
	// exit if not accept rule
	if len(policy.InlineLayer) == 0 && policy.Action != proc.AcceptObject {
		// fmt.Println("getFgPolicyRulesfromPolicy dropped not-accept-rule", policy.Name)
		return result
	}
	// exit if rule is configured to be ignored
	if proc.SkipRule(policy.Name) {
		return result
	}
	// Handle layered rules by replacing Any with the parent layers definition.
	if parent != nil {
		if len(policy.Service) == 1 && policy.Service[0] == proc.CpmiAnyObject {
			policy.Service = parent.Service
		}
		if len(policy.Source) == 1 && policy.Source[0] == proc.CpmiAnyObject {
			policy.Source = parent.Source
		}
		if len(policy.Destination) == 1 && policy.Destination[0] == proc.CpmiAnyObject {
			policy.Destination = parent.Destination
		}
	}
	// handle inline layer
	if len(policy.InlineLayer) > 0 {
		layObj := FindObjectByUID(&proc.Objects, policy.InlineLayer)
		layName := layObj["name"].(string)
		log.Println("getFgPolicyRulesfromPolicy loaded layer", layName)
		layFile, err := findFileInDir(proc.LayersPath, layName, "json")
		if err == nil {
			layRules, err := LoadObjectsFile(layFile)
			if err == nil {
				for _, layRule := range layRules {
					cpLayRule, _ := NewPolicy(layRule)
					result = append(result, proc.getFgPolicyRulesfromPolicy(cpLayRule, &policy)...)
				}
			} else {
				log.Println("getFgPolicyRulesfromPolicy: error loading layer:", err)
			}
		}
		return result
	}
	// continue if not a layer
	shouldNat, natPool := proc.NatRule(policy.Name)
	// make sure rule name is unique
	policy.Name = proc.GetUniqueFwPolicyName(policy.Name)
	// get variables
	srcZones := proc.ZoneMapper.FindZones(proc.GetIPFromGUID(policy.Source))
	dstZones := proc.ZoneMapper.FindZones(proc.GetIPFromGUID(policy.Destination))
	srcAddr := proc.NamesMapper.GetNames(proc.GetObjectNameFromGUID(policy.Source))
	dstAddr := proc.NamesMapper.GetNames(proc.GetObjectNameFromGUID(policy.Destination))
	services := proc.ServiceMapper.GetNames(proc.GetObjectNameFromGUID(policy.Service))
	onlyOneZone := (len(srcZones) == 1 && len(dstZones) == 1)
	fgpol := fortigate.Policy{
		Name:       policy.Name,
		Status:     "enable",
		SrcIntf:    fortigate.NewLinkedObjRefFromStrings(srcZones),
		DstIntf:    fortigate.NewLinkedObjRefFromStrings(dstZones),
		SrcAddr:    fortigate.NewLinkedObjRefFromStrings(srcAddr),
		DstAddr:    fortigate.NewLinkedObjRefFromStrings(dstAddr),
		Action:     "accept",
		Schedule:   "always",
		Service:    fortigate.NewLinkedObjRefFromStrings(services),
		LogTraffic: "all",
		Comment:    policy.Comment,
	}
	// enable NAT if configured
	if shouldNat {
		fgpol.NAT = "enable"
		if len(natPool) > 0 {
			fgpol.IPPool = "enable"
			fgpol.PoolName = fortigate.NewLinkedObjRefFromStrings([]string{natPool})
		}
	}
	// create rule
	if onlyOneZone {
		result = append(result, fgpol)
	} else {
		counter := 1
		for _, srcLoop := range srcZones {
			for _, dstLoop := range dstZones {
				if len(policy.Name) > MaxPolicyNameLength {
					fgpol.Comment = policy.Name
					fgpol.Name = policy.Name[0:MaxPolicyNameLength-1] + " " + strconv.Itoa(counter)
				} else {
					fgpol.Name = policy.Name + " " + strconv.Itoa(counter)
				}
				counter++
				if proc.ORule {
					fgpol.SrcAddr = fortigate.NewLinkedObjRefFromStrings(proc.NamesMapper.GetNames(proc.GetObjectNameFromGUID(proc.FilterGUIDInZone(policy.Source, srcLoop))))
					fgpol.DstAddr = fortigate.NewLinkedObjRefFromStrings(proc.NamesMapper.GetNames(proc.GetObjectNameFromGUID(proc.FilterGUIDInZone(policy.Destination, dstLoop))))
				}
				fgpol.SrcIntf = fortigate.NewLinkedObjRefFromStrings([]string{srcLoop})
				fgpol.DstIntf = fortigate.NewLinkedObjRefFromStrings([]string{dstLoop})
				result = append(result, fgpol)
			}
		}
	}
	return result
}

// CreateZoneListFromInterface identifies all interfaces (with an IP address) and adds it it to the zone list
func (pol *FwPolicyProcessor) CreateZoneListFromInterface(iflist map[string]fortigate.Interface) map[string][]string {
	var mymap = make(map[string][]string)
	for _, ifEntry := range iflist {
		if len(ifEntry.IP) > 0 && !utils.IsStringInStrings(ifEntry.Name, &pol.SkipInterfaces) {
			zone := pol.ZoneMapper.FindZone(ifEntry.IP)
			mymap[zone] = append(mymap[zone], ifEntry.Name)
		}
	}
	return mymap
}

// GetUniqueFwPolicyName checks to see if the name has been used before, and if so adds a unique number at the end
// of the rule. The rule is then truncated down do maxlen.
func (proc *FwPolicyProcessor) GetUniqueFwPolicyName(input string) (result string) {
	// see if name has been used before
	if _, ok := proc.UniqueFwNameMap[input]; ok {
		proc.UniqueFwNameMap[input] = proc.UniqueFwNameMap[input] + 1
		l := len(strconv.Itoa(proc.UniqueFwNameMap[input]))
		if len(input)+l > MaxPolicyNameLength {
			result = input[0:MaxPolicyNameLength-l-1] + strconv.Itoa(proc.UniqueFwNameMap[input])
		} else {
			result = input + strconv.Itoa(proc.UniqueFwNameMap[input])
		}
	} else { // name is new
		proc.UniqueFwNameMap[input] = 0
		if len(input) > MaxPolicyNameLength {
			result = input[0 : MaxPolicyNameLength-1]
		} else {
			result = input
		}
	}
	return
}

// SkipRule checks if name is in SkipRules, true if it is - false otherwise.
func (proc *FwPolicyProcessor) SkipRule(name string) bool {
	for _, v := range proc.SkipRules {
		if name == v {
			return true
		}
	}
	return false
}

// NatRule checks if name is in NATRules, true if it is - false otherwise.
// Return string is ip-pool to nat to, blank if none.
func (proc *FwPolicyProcessor) NatRule(name string) (bool, string) {
	// go through interface NAT
	for _, v := range proc.NATRules {
		if name == v {
			return true, ""
		}
	}
	// check ip-pool nat
	pool, nat := proc.NATPool[name]
	return nat, pool
}

// FilterGUIDInZone returns only the UID from the list that is part of the given zone.
// If a UID is a group the group will be expanded and the complete list returned.
func (proc *FwPolicyProcessor) FilterGUIDInZone(list []string, zone string) (result []string) {
	for _, item := range list {
		r := proc.GetIPFromGUID([]string{item})
		z := proc.ZoneMapper.FindZones(r)
		for _, v := range z {
			if v == zone {
				result = append(result, item)
				//break
			}
		}
	}
	return fortigate.RemoveDuplicates(result)
}
