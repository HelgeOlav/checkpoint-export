package checkpoint_export

import "testing"

func TestFwPolicyProcessor_CheckNames(t *testing.T) {
	pol := FwPolicyProcessor{SkipRules: []string{"A", "B"}, NATRules: []string{"C", "D"}}
	if pol.SkipRule("C") {
		t.Error("SkipRule expected false on C")
	}
	if pol.SkipRule("A") == false {
		t.Error("SkipRule expected true on A")
	}
	if pol.NatRule("A") {
		t.Error("NatRule expected false on A")
	}
	if pol.NatRule("C") == false {
		t.Error("NatRule expected true on C")
	}
}

func TestFwPolicyProcessor_GetUniqueFwPolicyName(t *testing.T) {
	pol := FwPolicyProcessor{UniqueFwNameMap: make(map[string]int)}
	// try same short name
	name := "ICMP"
	res := pol.GetUniqueFwPolicyName(name)
	if res != name {
		t.Errorf("GetUniqueFwPolicyName returned %s, expected %s", res, name)
	}
	// short name again
	res = pol.GetUniqueFwPolicyName(name)
	exp := name + "1"
	if res != exp {
		t.Errorf("GetUniqueFwPolicyName returned %s, expected %s", res, exp)
	}
	// long name
	name = "My very long policy name that should be truncated"
	res = pol.GetUniqueFwPolicyName(name)
	exp = name[0 : MaxPolicyNameLength-1]
	if res != exp {
		t.Errorf("GetUniqueFwPolicyName returned %s, expected %s", res, exp)
	}
	// and long name again
	res = pol.GetUniqueFwPolicyName(name)
	exp = name[0:MaxPolicyNameLength-2] + "1"
	if res != exp {
		t.Errorf("GetUniqueFwPolicyName returned %s, expected %s", res, exp)
	}
}
