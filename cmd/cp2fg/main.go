package main

import (
	checkpoint_export "bitbucket.org/HelgeOlav/checkpoint-export"
	"bitbucket.org/HelgeOlav/fortigate"
	"bitbucket.org/HelgeOlav/fortigate/zonemapper"
	"bitbucket.org/HelgeOlav/utils/version"
	"encoding/json"
	"log"
	"os"
)

// MapMap - map of maps - interface mapping, service mappings (optional) and address mappings (optional).
// Used with -map
type MapMap struct {
	Interfaces    map[string]string // mapping of names from Checkpoint to Fortigate
	Addresses     map[string]string // optional custom address/group mapping
	Services      map[string]string // optional custom service/group mapping
	IgnoreRule    []string          `json:"ignore-rule"`    // rules to ignore (based on name)
	NATRule       []string          `json:"nat-rule"`       // rules to enable NAT on (based on name)
	NATPool       map[string]string `json:"nat-pool"`       // rules with this name will be NAT'ed using the given pool (implies that it is a NAT rule)
	SkipInterface []string          `json:"skip-interface"` // interfaces to be skipped in output (zones, routes & interfaces)
}

var processor checkpoint_export.FwPolicyProcessor // processor is needed for rule migration but will also contain objects used elsewhere.
var topRules checkpoint_export.ArrayTop
var fgConnection fortigate.Connection

// readInput will read all input files specified and verify that they are readable.
func readInput() {
	var err error
	processor.LayersPath = *cmdLay // this parameter is not validated until needed as it looks for files inside that directory.
	// -obj
	processor.Objects, err = checkpoint_export.LoadObjectsFile(*cmdObj)
	if err != nil {
		log.Println("Error loading -obj", *cmdObj, "-", err)
		os.Exit(1)
	}
	// rule
	topRules, err = checkpoint_export.LoadObjectsFile(*cmdRule)
	if err != nil {
		log.Println("Error loading -rule", *cmdRule, "-", err)
		os.Exit(1)
	}
	// fg
	fgConnection, err = fortigate.LoadConnection(*cmdFg)
	if err != nil {
		log.Println("Error loading -fg", *cmdFg, "-", err)
		os.Exit(1)
	}
	// map
	processor.NamesMapper = checkpoint_export.NewDefaultAddressMapper()
	processor.ServiceMapper = checkpoint_export.NewDefaultServiceMapper()
	bytes, err := os.ReadFile(*cmdMap)
	if err != nil {
		log.Println("Error -map", err)
		os.Exit(1)
	}
	var mymap MapMap
	err = json.Unmarshal(bytes, &mymap)
	if err != nil {
		log.Println("Error parsing -map", err)
		os.Exit(1)
	}
	checkpoint_export.GaiaInterfaceMapper = mymap.Interfaces
	if len(mymap.Services) > 0 {
		newSrvMap := checkpoint_export.MapperService{
			Names: mymap.Services,
		}
		processor.ServiceMapper.NextMapper = &newSrvMap
	}
	if len(mymap.Addresses) > 0 {
		newSrvMap := checkpoint_export.MapperService{
			Names: mymap.Services,
		}
		processor.NamesMapper.NextMapper = &newSrvMap
	}
	processor.NATRules = mymap.NATRule
	processor.SkipRules = mymap.IgnoreRule
	processor.SkipInterfaces = mymap.SkipInterface
	processor.NATPool = mymap.NATPool
	// zone
	zm, err := zonemapper.LoadFromFile(*cmdZone)
	if err != nil {
		log.Println("Error loading -zone", *cmdZone, "-", err)
		os.Exit(1)
	}
	saveJsonToFile("routemapper.json", zm)
	processor.ZoneMapper = *zm
	log.Println("Read", len(processor.Objects), "objects and", len(topRules), "rules from input files.")
}

func main() {
	log.Println("cp2fg - one way to migrate from Checkpoint to Fortigate")
	log.Println(version.Get().LongString())
	cmdLine()      // check command line options
	readInput()    // open and read all input files
	createOutput() // and create the output we need
}
