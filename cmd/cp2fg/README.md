# Migrate Checkpoint to Fortigate

This tool is the main commandline tool to migrate firewall configuration. To migrate
the configuration you need to export configuration from Checkpoint and then run it though this tool
that will use the REST API to configure the Fortigate for you.

The following is migrated:

* Static routes.
* Interfaces. (With zone mapping.)
* ExternalZone for Internet.
* Address objects and groups. IPv4 only.
* Services and service groups.
* Firewall rules, including layers.

This is not migrated:

* NAT rules.
* Content rules. (Anything using blades.)
* Deny rules.
* Disabled rules.

### Export from Checkpoint

You need to do two separate exports from Checkpoint:

1. Export from [management](https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk120342#Output) is done running some scripts shown in the documentation. This is done using expert mode on the CLI.
1. Export from GAIA, using "show configuration" in CLI. 

Note! This has been tested on R80.30.

### Import into Fortigate

All import is done using the REST API. You need to make sure the API is available for import.
Before starting import you need to have a few things available:

1. A configuration file for specifying connection details towards the Fortigate.
1. A configuration file describing mapping of zones; ie what zone a interface and a subnet and an address(group) belongs to.
1. The dump from the Checkpoint as shown above.
1. (Optional) mapping of services/addresses. Using this mapping you can create objects manually on the Fortigate, and when those objects are seen they are renamed (in policy) and not created for service/address.

### Principles for migration

1. All services/address objects are created, even if not used. The only expection is if mapped away.
1. A rule is only for one source and one destination zone. If the source rule results in multiple source/dest several rules will be created. The rule name will then get a trailing counter.
1. There is no logic for creating group objects. If you have nested groups, you might have to run the migration tool several times to make sure everything gets in.
1. Interfaces that are not mapped are discarded. Only physical interfaces and their 802.1q subinterfaces are migrated.

## How to use this tool

First you need to download and build it. That process is not described here.

The tool is run using many command line options. Some are to specify input data, others to specify what it will do.

Many command line parameters can be specified in the file cp2fg.json found in the working directory, or the file specified
by the environment variable CP2FG. Syntax for the file can be seen in cmdline.go and CmdLineOpts struct.

### Input parameters

All parameters are needed even if it is not needed for the output.

* ```-gaia filename``` specifies what file contains the output from Gaia "show configuration".
* ```-obj filename``` specifies the file that contains objects from Checkpoint (addresses/services etc).
* ```-rule filename``` is the Checkpoint export file with the rules you want to migrate.
* ```-fg filename``` is the Fortigate connection parameters.
* ```-map filename``` Specify map for renaming interfaces, services and addresses.
* ```-zone filename``` zone mappings for creating rules.
* ```-lay filename``` path to export directory where all layers are stored. (Usually the directory where you have all export files from the Checkpoint.)

### Output commands

These are the commands produce output. If you specify more than one command they are run in the order shown below.

* ```-ca``` create address objects
* ```-cag``` create address groups
* ```-cs``` create service objects
* ```-csg``` create service groups
* ```-ci``` create interfaces
* ```-cz``` create zones
* ```-cr``` create rules
